<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\RatesModel;
use Model\UsersModel;

class RatesController implements ControllerProviderInterface
{
    protected $_model;

    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new RatesModel($app);
        $this->_user = new UsersModel($app);
        $rateController = $app['controllers_factory'];
        $rateController->get('/{page}/', array($this, 'index'))->value('page', 1)->bind('/rates/');
        $rateController->match('/add/{idproject}', array($this, 'add'))->bind('/rates/add');
        $rateController->match('/view/{idproject}', array($this, 'generalRate'))->bind('/rates/view');
        return $rateController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $rates = $this->_model->getStatystic();

        return $app['twig']->render('rates/index.twig', array('rates' => $rates));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {
        $idproject = (int) $request->get('idproject', 0);

        $iduser = $this->getIdCurrentUser($app);

        $check = $this->_model->checkAccess($idproject, $iduser);

        if($check){
            $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'You are alredy add your rate'));
            return $app->redirect($app['url_generator']->generate("/projects/"), 301);
        }
        // default values:
        $data = array(
            'published_date'=> date('Y-m-d'),
            'idproject'=> $idproject,
            'iduser'=> $iduser,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('rate', 'choice', array(
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                ),
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $model= $this->_model->addRate($data);
            if(!$model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New rate added.'));
                return $app->redirect($app['url_generator']->generate("/projects/"), 301);
            }
        }

        return $app['twig']->render('rates/add.twig', array('form' => $form->createView()));
    }


    public function generalRate(Application $app, Request $request)
    {
        $idproject = (int) $request->get('idproject', 0);

        $rate = $this->_model->getGeneralRate($idproject);

        $generalRate = number_format($rate['general'], 2);

        return $app['twig']->render('rates/view.twig', array('idproject'=> $idproject, 'rate'=>$generalRate));
    }

    public function getIdCurrentUser($app)
    {

        $login = $this->getCurrentUser($app);
        $iduser = $this->_user->getUserByLogin($login);

        return $iduser['iduser'];


    }

    protected function getCurrentUser($app)
    {
        $token = $app['security']->getToken();

        if (null !== $token) {
            $user = $token->getUser()->getUsername();
        }

        return $user;
    }
}