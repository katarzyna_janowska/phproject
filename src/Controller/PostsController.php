<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\PostsModel;
use Model\TagsModel;
use Model\CategoriesModel;

class PostsController implements ControllerProviderInterface
{
    protected $_model;
    protected $_tags;
    protected $_category;


    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new PostsModel($app);
        $this->_tags = new TagsModel($app);
        $this->_category = new CategoriesModel($app);
        $postController = $app['controllers_factory'];
        $postController->get('/{page}', array($this, 'index'))->value('page', 1)->bind('/posts/');
        $postController->match('/add/', array($this, 'add'))->bind('/posts/add');
        $postController->match('/edit/{id}', array($this, 'edit'))->bind('/posts/edit');;
        $postController->match('/delete/{id}', array($this, 'delete'))->bind('/posts/delete');;
        $postController->get('/view/{id}', array($this, 'view'))->bind('/posts/view');
        return $postController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $pageLimit = 1;
        $page = (int) $request->get('page', 1);
        $pagesCount = $this->_model->countPostsPages($pageLimit);
        if (($page < 1) || ($page > $pagesCount)) {
            $page = 1;
        }
        $posts = $this->_model->getPostsPage($page, $pageLimit, $pagesCount);
        $paginator = array('page' => $page, 'pagesCount' => $pagesCount);
        return $app['twig']->render('posts/index.twig', array('posts' => $posts, 'paginator' => $paginator));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {
        $categories = $this->_category->getCategoriesDict();

        // default values:
        $data = array(
            'published_date'=> date('Y-m-d'),
            'idcategory' =>$categories,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('content', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('pulished_date', 'date', array(
                'input'  => 'string',
                'widget' => 'choice',
            ))
            ->add('category', 'choice', array(
                'choices'=> $categories,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $model= $this->_model->addPost($data);
            if($model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New post added.'));
                return $app->redirect($app['url_generator']->generate('/posts/'), 301);
            }
        }

        return $app['twig']->render('posts/add.twig', array('form' => $form->createView()));
    }

    /*
     *
     */
    public function edit(Application $app, Request $request)
    {
        $categories = $this->_category->getCategoriesDict();

        $id = (int) $request->get('id', 0);

        $post = $this->_model->getPost($id);

        // default values:
        $data = array(
            'title' => $post['title'],
            'content' => $post['content'],
            'published_date'=> $post['published_date'],
            'idcategory' => $post['idcategory'],
        );

        if(count($post)){
            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('id', 'hidden', array(
                    'data'=> $id,
                ))
                ->add('title', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('content', 'textarea', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('pulished_date', 'date', array(
                    'input'  => 'string',
                    'widget' => 'choice',
                ))
                ->add('category', 'choice', array(
                    'choices'=> $categories,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $model= $this->_model->editPost($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'post update.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('posts/edit.twig', array('form' => $form->createView(), 'idpost'=>$id ));
        }else{
            return $app->redirect($app['url_generator']->generate('/posts/add'), 301);
        }
    }

    /*
     *
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);

        $post = $this->_model->getPost($id);

        $data=array();

        if (count($post)) {
            $redirect = $app->redirect($app['url_generator']->generate('/posts/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idpost', 'hidden', array(
                    'data'=>$id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model= $this->_model->deletePost($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'post delete.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('posts/delete.twig', array('form' => $form->createView(), 'redirect'=>$redirect));

        }else{
            $app->notFound();
        }
    }

    /*
     *
     */
    public function view(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);

        $post = $this->_model->getPostWithCategoryName($id);
//var_dump(app.security.getToken().getUser().getUsername());
        $tags = $this->_tags->getTagsListByPost($id);

            if (count($post)) {
                return $app['twig']->render('posts/view.twig', array('post' => $post, 'tags'=>$tags));
            }else{
                $app->notFound();
            }
    }



}