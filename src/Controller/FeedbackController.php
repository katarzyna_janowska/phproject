<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\FeedbackModel;
use Model\UsersModel;

class FeedbackController implements ControllerProviderInterface
{
    protected $_model;
    protected $_user;


    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new feedbackModel($app);
        $this->_user = new UsersModel($app);
        $feedbackController = $app['controllers_factory'];
        $feedbackController->get('/{page}/{idproject}/', array($this, 'index'))->value('page', 1)->bind('/feedback/');
        $feedbackController->match('/add/{idproject}', array($this, 'add'))->bind('/feedback/add');
        $feedbackController->match('/edit/{id}', array($this, 'edit'))->bind('/feedback/edit');;
        $feedbackController->match('/delete/{id}', array($this, 'delete'))->bind('/feedback/delete');;
        return $feedbackController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $id = (int) $request->get('idproject', 0);

        $feedback = $this->_model->getFeedbackList($id);

        return $app['twig']->render('feedback/index.twig', array('feedback' => $feedback, 'idproject'=> $id));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {

        $idpost = (int) $request->get('idproject', 0);
        $iduser = $this->getIdCurrentUser($app);


        // default values:
        $data = array(
            'published_date'=> date('Y-m-d'),
            'idproject'=>$idpost,
            'iduser'=>$iduser,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('content', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $model= $this->_model->addFeedback($data);
            if(!$model){
                echo 'Your feedback was saccesfully added';
            }
        }

        return $app['twig']->render('feedback/add.twig', array('form' => $form->createView(), 'idpost'=>$idpost));
    }

    /*
     *
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);
        $idCurrentUser = $this->getIdCurrentUser($app);
        $feedback = $this->_model->getFeedback($id);

        // default values:
        $data = array(
            'idfeedback'=>$id,
            'published_date'=>date('Y-m-d'),
            'idproject'=>$feedback['idproject'],
            'iduser'=>$feedback['iduser'],
            'idCurrentUser'=>$idCurrentUser,
        );

        if(count($feedback)){
            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('content', 'textarea', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                var_dump($data);
                $model= $this->_model->editFeedback($data);
                if(!$model){
                    echo 'Your feedback was saccesfully updated';
                }
            }

            return $app['twig']->render('feedback/edit.twig', array('form' => $form->createView()));
        }else{
            return $app->redirect($app['url_generator']->generate('/feedback/add'), 301);
        }
    }

    /*
     *
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);

        $feedback = $this->_model->getFeedback($id);

        $data=array();

        if (count($feedback)) {
            $redirect = $app->redirect($app['url_generator']->generate('/feedback/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idfeedback', 'hidden', array(
                    'data'=>$id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model= $this->_model->deleteFeedback($data);
                if(!$model){
                    echo 'Your feedback was saccesfully delete';
                }
            }

            return $app['twig']->render('feedback/delete.twig', array('form' => $form->createView(), 'redirect'=>$redirect));

        }else{
            $app->notFound();
        }
    }


    public function getIdCurrentUser($app)
    {

        $login = $this->getCurrentUser($app);
        $iduser = $this->_user->getUserByLogin($login);

        return $iduser['iduser'];


    }

    protected function getCurrentUser($app)
    {
        $token = $app['security']->getToken();

        if (null !== $token) {
            $user = $token->getUser()->getUsername();
        }

        return $user;
    }


}