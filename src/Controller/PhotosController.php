<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Model\PhotosModel;

class PhotosController implements  ControllerProviderInterface
{
    protected $_model;

    public function connect(Application $app)
    {
        $this->_model = new PhotosModel($app);
        $photosController = $app['controllers_factory'];
        $photosController->get('/{page}/{idproject}/', array($this, 'index'))->value('page', 1)->bind('/photos/');
        $photosController->match('/upload/{idproject}', array($this, 'upload'))->bind('/photos/upload');
        $photosController->match('/delete/{name}', array($this, 'delete'))->bind('/photos/delete');;
        $photosController->get('/view/{name}', array($this, 'view'))->bind('/photos/view');
        return $photosController;
    }


    public function index(Application $app, Request $request )
    {
        $idproject =  $id = (int) $request->get('idproject', 0);

        $photos = $this->_model->getPhotosByProject($idproject);

        return $app['twig']->render('photos/index.twig', array('photos'=> $photos));

    }


    public function upload(Application $app, Request $request)
    {



        $idproject =  $id = (int) $request->get('idproject', 0);



        $data = array(
          'idproject'=> $idproject,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('file', 'file', array('label' => 'Choose file'))
            ->add('alt', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('save', 'submit', array('label' => 'Upload file'))
            ->getForm();
        if ($request->isMethod('POST'))
        {
            $form->bind($request);

            if ($form->isValid()) {

                try {

                    $files = $request->files->get($form->getName());
                    $data = $form->getData();

                    $path = dirname(dirname(dirname(__FILE__))).'/web/media';

                    $originalFilename = $files['file']->getClientOriginalName();

                    $newFilename = $this->_model->createName($originalFilename);
                    $files['file']->move($path, $newFilename);

                    $this->_model->savePhoto($newFilename, $data);

                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'File successfully uploaded.'));

                } catch (Exception $e) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'error', 'content' => 'Can not upload file.'));
                }

            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'content' => 'Form contains invalid data.'));
            }

        }

        return $app['twig']->render('photos/upload.twig', array('form' => $form->createView(), 'idproject'=> $idproject));
    }

}