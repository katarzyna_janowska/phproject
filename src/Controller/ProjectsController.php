<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\ProjectsModel;

class ProjectsController implements ControllerProviderInterface
{
    protected $_model;

    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new ProjectsModel($app);
        $projectController = $app['controllers_factory'];
        $projectController->get('/{page}', array($this, 'index'))->value('page', 1)->bind('/projects/');
        $projectController->match('/add/', array($this, 'add'))->bind('/projects/add');
        $projectController->match('/edit/{id}', array($this, 'edit'))->bind('/projects/edit');
        $projectController->match('/delete/{id}', array($this, 'delete'))->bind('/projects/delete');;
        $projectController->get('/view/{id}', array($this, 'view'))->bind('/projects/view');
        return $projectController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $pageLimit = 1;
        $page = (int) $request->get('page', 1);
        $pagesCount = $this->_model->countProjectsPages($pageLimit);
        if (($page < 1) || ($page > $pagesCount)) {
            $page = 1;
        }
        $projects = $this->_model->getProjectsPage($page, $pageLimit, $pagesCount);
        $paginator = array('page' => $page, 'pagesCount' => $pagesCount);
        return $app['twig']->render('projects/index.twig', array('projects' => $projects, 'paginator' => $paginator));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {
        // default values:
        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('description', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $model= $this->_model->addProject($data);
            if(!$model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New project added.'));
                return $app->redirect($app['url_generator']->generate("/projects/"), 301);
            }
        }

        return $app['twig']->render('projects/add.twig', array('form' => $form->createView()));
    }

    /*
     *
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);

        $project = $this->_model->getProject($id);

        // default values:
        $data = array(
            'title' => $project['title'],
            'description' => $project['description'],
        );

        if(count($project)){
            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('id', 'hidden', array(
                    'data'=> $id,
                ))
                ->add('title', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('description', 'textarea', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $model= $this->_model->editProject($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'post update.'));
                    return $app->redirect($app['url_generator']->generate("/projects/"), 301);
                }
            }

            return $app['twig']->render('projects/edit.twig', array('form' => $form->createView()));
        }else{
            return $app->redirect($app['url_generator']->generate('/projects/add'), 301);
        }
    }

    /*
     *
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);

        $project = $this->_model->getProject($id);

        $data=array();

        if (count($project)) {
            $redirect = $app->redirect($app['url_generator']->generate('/projects/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idproject', 'hidden', array(
                    'data'=>$id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model= $this->_model->deleteProject($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'project delete'));
                    return $app->redirect($app['url_generator']->generate("/projects/"), 301);
                }
            }

            return $app['twig']->render('projects/delete.twig', array('form' => $form->createView(), 'redirect'=>$redirect));

        }else{
            $app->notFound();
        }
    }

    /*
     *
     */
    public function view(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);

        $project = $this->_model->getProject($id);
        if (count($project)) {
            return $app['twig']->render('projects/view.twig', array('project' => $project));
        }else{
            $app->notFound();
        }
    }



}