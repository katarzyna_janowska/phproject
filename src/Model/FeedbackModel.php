<?php

namespace Model;

use Silex\Application;

/**
 * Class FeedbackModel
 *
 * @class FeedbackModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class FeedbackModel
{

    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one feedback.
     *
     * @access public
     * @param Integer $idfeedback
     * @return Array Associative array contains all information about this one feedback.
     */
    public function getFeedback($idfeedback)
    {
        $sql = 'SELECT * FROM project_feedback WHERE idfeedback = ? LIMIT 1';
        return $this->_db->fetchAssoc($sql, array($idfeedback));
    }

    /**
     * Gets all feedback for one project
     *
     * @access public
     * @param Integer $idpost
     * @return Array Feedback array
     */
    public function getFeedbackList($id)
    {
        $sql = 'SELECT * FROM project_feedback WHERE idproject = ?';
        return $this->_db->fetchAll($sql, array($id));
    }

    /**
     * Puts one feedback.
     *
     * @access public
     * @param  Array $data Associative array contains id feedback, feedbacks content, pubished date, idproject and id logged user.
     * @return Void
     */
    public function addFeedback($data)
    {
        $sql = 'INSERT INTO project_feedback (content, published_date, idproject, iduser) VALUES (?,?,?,?)';
        $this->_db->executeQuery($sql, array($data['content'],  $data['published_date'], $data['idproject'], $data['iduser']));
    }

    /**
     * Updates one feedback.
     *
     * @access public
     * @param Array $data Associative array contains id feedback, feedbacks content, pubished date, idproject and id logged user.
     * @return Void
     */
    public function editFeedback($data)
    {

        if (isset($data['idfeedback']) && ctype_digit((string)$data['idfeedback'])) {
            $sql = 'UPDATE project_feedback SET content = ?, published_date = ? WHERE idfeedback = ?';
            $this->_db->executeQuery($sql, array($data['content'],  $data['published_date'], $data['idfeedback']));
        }else{
            $sql = 'INSERT INTO project_feedback (content, published_date, idproject, iduser) VALUES (?,?,?,?)';
            $this->_db->executeQuery($sql, array($data['content'],  $data['published_date'], $data['idproject'], $data['idCurrentUser']));
        }
    }

    /**
     * Delete one comment.
     *
     * @access public
     * @param Array $data Associative array contains  id feedback.
     * @return Void
     */
    public function deleteFeedback($data)
    {
        $sql = 'DELETE FROM `project_feedback` WHERE `idfeedback`= ?';
        $this->_db->executeQuery($sql, array($data['idfeedback']));
    }

}