<?php

namespace Model;

use Silex\Application;

class PagesModel
{

    protected $_db;

    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    public function getPage($name)
    {
        $sql = 'SELECT * FROM pages WHERE title = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($name));
    }

    public function getInformation($idpage)
    {
        $sql = 'SELECT * FROM pages_attributes right join pages_values on pages_attributes.idattribute = pages_values.idattribute where idpage = ?;';
        return $this->_db->fetchAll($sql, array($idpage));
    }

    public function updatePage($data)
    {
        $pageValues = "SELECT * FROM pages_attributes right join pages_values on pages_attributes.idattribute = pages_values.idattribute where idpage = ?";
        $attriubutesNames = $this->_db->fetchAll($pageValues, array($data['idpage']));

        foreach ($attriubutesNames as $attribute) {
            foreach ($data as $key => $value) {
                if ($attribute['title'] == $key) {
                    $sql = "UPDATE `pages_values` SET content = ? WHERE `idpage`= ?  and `idattribute`=?";
                    $this->_db->executeQuery($sql, array($value, $data['idpage'], $attribute['idattribute']));
                }
            }
        }
        return true;
    }

    public function updateContact($data)
    {
        $pageValues = "SELECT * FROM pages_attributes right join pages_values on pages_attributes.idattribute = pages_values.idattribute where idpage = 2";
        $attriubutesNames = $this->_db->fetchAll($pageValues);

        foreach ($attriubutesNames as $attribute) {
            foreach ($data as $key => $value) {
                if ($attribute['title'] == $key) {
                    $sql = "UPDATE `pages_values` SET content = ? WHERE `idpage`= 2  and `idattribute`=?";
                    $this->_db->executeQuery($sql, array($value, $attribute['idattribute']));
                }
            }
        }
        return true;
    }

    public function getFields($idpage)
    {
        $sql = "SELECT * FROM pages_values natural join pages_attributes where idpage = ?;";
        return $this->_db->fetchAll($sql, array($idpage));
    }

    /**
     * Change key in categories array
     *
     * @access public
     * @return Array tags array.
     */

    public function getPageDict()
    {
        $categories = $this->getCategories();
        $data = array();
        foreach ($categories as $row) {
            $data[$row['idcategory']] = $row['name'];
        }
        return $data;
    }

}