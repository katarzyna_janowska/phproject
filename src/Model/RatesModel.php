<?php

namespace Model;

use Silex\Application;

/**
 * Class RatesModel
 *
 * @class RatesModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class RatesModel
{

    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one rate.
     *
     * @access public
     * @param Integer $idRate
     * @return Array Associative array contains all information about this one rate.
     */
    public function getRate($idRate)
    {
        $sql = 'SELECT * FROM project_ratings WHERE id = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($idRate));
    }

    /**
     * Gets general projects rate .
     *
     * @access public
     * @param Integer $idproject
     * @return Array Associative array with general rate.
     */
    public function getGeneralRate($idproject)
    {
        $sql = "SELECT avg(rate) as general FROM project_ratings where idproject = ? LIMIT 1;";
        return $this->_db->fetchAssoc($sql, array($idproject));
    }

    /**
     * Puts rate.
     *
     * @access public
     * @param  Array $data Associative array contains information about rate: rate, published date, idproject and id logged user.
     * @return Void
     */
    public function addRate($data)
    {
        $sql = 'INSERT INTO project_ratings (rate, published_date, idproject, iduser) VALUES (?,?,?,?)';
        $this->_db->executeQuery($sql, array($data['rate'], $data['published_date'], $data['idproject'], $data['iduser']));
    }

    /**
     * Gets general rates for all projects
     *
     * @access public
     * @return Array projects array with general rates.
     */
    public function getStatystic()
    {
        $sql = "SELECT projects.idproject, title,  avg(rate) as general FROM project_ratings right join projects on project_ratings.idproject = projects.idproject order by general;";
        return $this->_db->fetchAll($sql);
    }

    /**
     * Check if user added his rate already.
     *
     * @access public
     * @param Integer $idproject
     * @param Integer $iduser
     * @return Array projects array with general rates.
     */
    public function checkAccess($idproject, $iduser)
    {
        $sql = "SELECT * FROM project_ratings where idproject = ? && iduser = ?;";
        $result = $this->_db->fetchAll($sql, array($idproject, $iduser));

        if($result){
            return true;
        }else{
            return false;
        }
    }
}