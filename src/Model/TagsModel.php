<?php

namespace Model;

use Silex\Application;

/**
 * Class TagsModel
 *
 * @class TagsModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class TagsModel
{

    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one tag.
     *
     * @access public
     * @param Integer $idTag
     * @return Array Associative array contains all information about this one tag.
     */
    public function getTag($idTag)
    {
        $sql = 'SELECT * FROM blog_tags WHERE idtag = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($idTag));
    }

    /**
     * Puts tag.
     *
     * @access public
     * @param  Array $data Associative array contains new tags title.
     * @return Void
     */
    public function addTag($data)
    {
        $sql = 'INSERT INTO blog_tags (title) VALUES (?)';
        $this->_db->executeQuery($sql, array($data['title']));
    }

    /**
     * Updates name of tag.
     *
     * @access public
     * @param Array $data Associative array contains id tag and new title.
     * @return Void
     */
    public function editTag($data)
    {

        if (isset($data['idtag']) && ctype_digit((string)$data['idtag'])) {
            $sql = 'UPDATE blog_tags SET title = ? WHERE idtag = ?';
            $this->_db->executeQuery($sql, array($data['title'],  $data['idtag']));
        }else{
            $sql = 'INSERT INTO blog_tags (idtag, title) VALUES (?,?)';
            $this->_db->executeQuery($sql, array($data['idtag'],  $data['title']));
        }
    }

    /**
     * Delete tag.
     *
     * @access public
     * @param Array $data Associative array contains id tag.
     * @return Void
     */
    public function deleteTag($data)
    {
        $sql = 'DELETE FROM `blog_tags` WHERE `idtag`= ?';
        $this->_db->executeQuery($sql, array($data['idtag']));
    }

    /**
     * Gets all tags for post.
     *
     * @access public
     * @param Integer $id Post id
     * @return Array Tags array.
     */
    public function getTagsListByPost($id)
    {
        $sql = 'SELECT * FROM blog_posts_tags natural join blog_tags WHERE idpost = ?';
        return $this->_db->fetchAll($sql, array($id));
    }

    /**
     * Change key in tags array
     *
     * @access public
     * @return Array tags array.
     */
    public function getTagsDict()
    {
        $categories = $this->getTagList();
        $data = array();
        foreach($categories as $row) {
            $data[$row['idtag']] = $row['title'];
        }
        return $data;
    }

    /**
     * Connect tag with post
     *
     * @access public
     * @param Array $data Array contains id post and id tag
     * @return Void
     */
    public function connectWithPost($data)
    {
        $sql = "INSERT INTO `blog_posts_tags` (`idpost`, `idtag`) VALUES (?, ?);";

        $this->_db->executeQuery($sql, array($data['idpost'], $data['idtag']));
    }

    /**
     * Disconnect tag with post
     *
     * @access public
     * @param Array $data Array contains id post and id tag
     * @return Void
     */
    public function disconnectWithPost($data)
    {
        $sql = "DELETE FROM `blog_posts_tags` WHERE `idpost`= ? && `idtag`= ?;";
        $this->_db->executeQuery($sql, array($data['idpost'], $data['idtag']));
    }

    /**
     * Gets general rates for all posts
     *
     * @access public
     * @return Array tags array.
     */
    public  function getTagList()
    {
        $sql = 'SELECT * FROM blog_tags';
        return $this->_db->fetchAll($sql);
    }

}