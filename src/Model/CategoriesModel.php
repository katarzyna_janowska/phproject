<?php

namespace Model;

use Silex\Application;

/**
 * Class CategoriesModel
 *
 * @class CategoriesModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class CategoriesModel
{

    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one category.
     *
     * @access public
     * @param Integer $idCategory
     * @return Array Associative array contains all information about this one tag.
     */
    public function getCategory($idCategory)
    {
        $sql = 'SELECT * FROM blog_categories WHERE idcategory = ? LIMIT 1';
        return $this->_db->fetchAssoc($sql, array($idCategory));
    }

    public function getPostsListByIdcategory($id)
    {
        $sql = 'SELECT * FROM blog_posts natural join blog_categories where idcategory = ?';
        return $this->_db->fetchAll($sql, array($id));
    }
    /**
     * Add category.
     *
     * @access public
     * @param  Array $data Associative array contains new category name.
     * @return Void
     */
    public function addCategory($data)
    {
        $sql = 'INSERT INTO blog_categories (name) VALUES (?)';
        $this->_db->executeQuery($sql, array($data['name']));
    }

    /**
     * Updates name of category.
     *
     * @access public
     * @param Array $data Associative array contains id category and new name.
     * @return Void
     */
    public function editCategory($data)
    {

        if (isset($data['idcategory']) && ctype_digit((string)$data['idcategory'])) {
            $sql = 'UPDATE blog_categories SET name = ? WHERE idcategory = ?';
            $this->_db->executeQuery($sql, array($data['name'],  $data['idcategory']));
        }else{
            $sql = 'INSERT INTO blog_categories (idcategory, name) VALUES (?,?)';
            $this->_db->executeQuery($sql, array($data['idcategory'],  $data['name']));
        }
    }

    /**
     * Delete category.
     *
     * @access public
     * @param Array $data Associative array contains id category.
     * @return Void
     */
    public function deleteCategory($data)
    {
        $sql = 'DELETE FROM `blog_categories` WHERE `idcategory`= ?';
        $this->_db->executeQuery($sql, array($data['idcategory']));
    }

    /**
     * Change key in categories array
     *
     * @access public
     * @return Array tags array.
     */

    public function getCategoriesDict()
    {
        $categories = $this->getCategories();
        $data = array();
        foreach($categories as $row) {
            $data[$row['idcategory']] = $row['name'];
        }
        return $data;
    }


    /**
     * Gets all categories.
     *
     * @access public
     * @return Array Categories array.
     */
    public  function getCategories()
    {
        $sql = 'SELECT * FROM blog_categories';
        return $this->_db->fetchAll($sql);
    }

}